#!/usr/bin/env python
import json
import sys

if len(sys.argv) != 2:
    print('usage: ./parse_articles.py articles.json')
    exit(-1)


data = None
with open(sys.argv[1]) as file:
    data = json.load(file)

added_coins = [
        a['title'].split()[2] for a in data['articles'] 
        if 'binance adds' in a['title'].lower() 
    ]

print(added_coins)

